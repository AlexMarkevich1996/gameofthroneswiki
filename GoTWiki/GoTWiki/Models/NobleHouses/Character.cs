﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GoTWiki.Models
{
    public class Character
    {
        public int Id { get; set; }
        public string Name { get; set; }

        public int CharacterId { get; set; }
        public NobleHouse NobleHouse { get; set; }
    }
}
