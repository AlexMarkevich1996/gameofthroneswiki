﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using GoTWiki.Models;
using Microsoft.AspNetCore.Authorization;
using System.Security.Claims;

namespace GoTWiki.Controllers
{
    
    public class HomeController : Controller
    {
        public IActionResult Index()
        {
            return View();
        }
        

        [Route("Bran_Stark")]
        public IActionResult BranStark()
        {
            return View();
        }
    }
}
