﻿using System;
using System.Collections.Generic;
using System.Text;
using GoTWiki.Models;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;

namespace GoTWiki.Data
{
    public class ApplicationDbContext : IdentityDbContext
    {
        public DbSet<NobleHouse> NobleHouses { get; set; }
        public DbSet<Character> Characters { get; set; }

        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options) : base(options)
        {
            Database.EnsureCreated();
        }
    }
}
